// Home Controller which act as a intermediate between home page and angular.
clockApp.controller('homeCtrl',function($scope,$interval){
    
    
    //using the date function to get the current date and time.
    $scope.date=new Date();
    
    //The interval service is used to execute the task for every seconds
    $interval(function(){
        $scope.date=new Date();
        
    },1000);
});



//World clock Controller is created and used for the workclock page where it uses the date function and gets the Universal date.

clockApp.controller('worldClockCtrl',function($scope,$routeParams,$interval){
    
    $scope.id=$routeParams.id;
    $scope.name=$routeParams.name; 

    //the interval service is used to execute the date function for specified intervals.
    
    var interval=$interval(function(){
       //  $scope.date1=new Date().toUTCString();
   // $scope.date2=new Date($scope.date1).toUTCString();
    if($scope.name=="Africa,Maputo"){
        $scope.dt=new Date().toISOString();
        //$scope.date2.setHours($scope.date1.getHours()+2);
            
        }
    else if($scope.name=="Australia,Broken_Hill"){
        $scope.dt=new Date().toISOString();
       // $scope.date2.setHours($scope.date1.getHours()-4);
    }
    else if($scope.name== "Europe,Madrid"){
        $scope.dt=new Date().toISOString();
        //$scope.date2.setHours($scope.date1.getHours()+1);
    }
    else if($scope.name== "Asia,Anadyr"){
        $scope.dt=new Date().toISOString();
        //$scope.date2.setHours($scope.date1.getHours()+1);
    }
    else if($scope.name=="Indian,Reunion"){
        $scope.dt=new Date().toISOString();        
       /* $scope.date2.setHours($scope.date1.getHours()+10);
        $scope.date2.getMinutes($scope.date1.getMinutes()+30);  */
    }
    },1000);
    //array variable which contains the list of countries with spcific id.
    $scope.countries=[{
      id:16,
        name:"Africa,Maputo"
    },{
        id:1315,
        name:"Asia,Anadyr"
        
    },{
        id:146,
        name:"Australia,Broken_Hill"
    },{
        id:187,
        name:"Europe,Madrid"
        
    },{
        id:750,
        name:"Indian,Reunion"
    }];
    });



//The timer controller is used to create a timer and performs the process of timer application.
//Here it uses $window to display the message and uses $location to redirect to the specified page.


clockApp.controller('timerCtrl',function($scope,$interval,$window,$routeParams,$location){
    $scope.hours=$routeParams.hours || "";
    $scope.minutes=$routeParams.minutes || "";
    $scope.seconds=$routeParams.seconds || "";
    
    //It executes the if when the parameters are not null which triggers the interval. It is invoked when the Timer is Started
    if($scope.hours != "" && $scope.minutes !="" && $scope.seconds !="")
        {   
            
            //Here we use $interval to perform the specified function of timer in a given time interval.
            
              var interval=$interval(function(){
                  //It checks seconds and updates it
            if($scope.seconds >0 && $scope.seconds <=59)
                {
                    $scope.seconds-=1;
                    var hours=$scope.hours;
                    var minutes=$scope.minutes;
                    var seconds=$scope.seconds;
                    var isEnabled=$scope.isEnabled;
                    $location.path("/timer/"+hours+"/"+minutes+"/"+seconds);
                  //  $location.path('/timer/{{hours}}/{{minutes}}/{{seconds}}');
                }
                  //It checks minutes and updates it
            else if($scope.seconds == 0 && $scope.minutes >0 && $scope.minutes <=59){
                $scope.seconds=59;
                $scope.minutes-=1;
                
                                var hours=$scope.hours;
                    var minutes=$scope.minutes;
                    var seconds=$scope.seconds;
                     var isEnabled=$scope.isEnabled;
                    $location.path("/timer/"+hours+"/"+minutes+"/"+seconds);
               // $location.path('/timer/{{hours}}/{{minutes}}/{{seconds}}');
            }
                  
                  //It checks hours and updates it.
            else if($scope.seconds == 0 && $scope.minutes == 0 && $scope.hours>0 && $scope.hours <=23){
                $scope.seconds=59;
                $scope.minutes=59;
                $scope.hours-=1;
                     var hours=$scope.hours;
                    var minutes=$scope.minutes;
                    var seconds=$scope.seconds;
                     var isEnabled=$scope.isEnabled;
                    $location.path("/timer/"+hours+"/"+minutes+"/"+seconds);
              //  $location.path('/timer/{{hours}}/{{minutes}}/{{seconds}}');
            }
                  //It checks whether timer has ended and if condition is true stops the Timer.
            else if(($scope.seconds == 0 || $scope.seconds=="") && ($scope.minutes == 0 ||$scope.seconds=="" )&& ($scope.hours == 0|| $scope.hours=="")){
                
                //here we use $interval.cancel() to cancel the $interval because we need to stop the timer when it reaches the end value 
                //But the $interval keep on running.To stop this we ue $interval.cancel() to stop
                
                 $interval.cancel(interval);
                
                //displayd the message when the timer ends
                
                $window.alert("Time\'s out");
                
                //redirects to the specified path
                
                $location.path('/timer');
                
                 
                 
            }
        
    },1000);  
             
        }


    $scope.hr=[{id:00},{id:01},{id:02},{id:03},{id:04},{id:05}
              ,{id:06},{id:07},{id:08},{id:09},{id:10}
              ,{id:11},{id:12},{id:13},{id:14},{id:15}
              ,{id:16},{id:17},{id:18},{id:19},{id:20}
              ,{id:21},{id:22},{id:23}];
    $scope.min=[{id:00},{id:01},{id:02},{id:03},{id:04},{id:05}
              ,{id:06},{id:07},{id:08},{id:09},{id:10}
              ,{id:11},{id:12},{id:13},{id:14},{id:15}
              ,{id:16},{id:17},{id:18},{id:19},{id:20}
              ,{id:21},{id:22},{id:23},{id:24},{id:25}
              ,{id:26},{id:27},{id:28},{id:29},{id:30},{id:31}
              ,{id:32},{id:33},{id:34},{id:35},{id:36}
              ,{id:37},{id:38},{id:39},{id:40},{id:41}
              ,{id:42},{id:43},{id:44},{id:45},{id:46}
              ,{id:47},{id:48},{id:49},{id:50},{id:51}
               ,{id:52},{id:53},{id:54},{id:55},{id:56}
               ,{id:57},{id:58},{id:59}];
    

 //Function is called or triggered when the user manually stops the timer.
        
$scope.stopTimer=function(){
    $scope.isEnabled=false;
    $interval.cancel(interval); 
    $location.path('/timer');
}

    
});
    