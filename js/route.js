//Using the routeProvider to rediect the page during the execution of the application

clockApp.config(function($routeProvider){
    //.when() contains two parameters where first parameter is the link and second parameter executes the code based on first argument.
    
    $routeProvider
    .when('/',{
    templateUrl:'html/home.html',
    controller:'homeCtrl'
    })
    .when('/timer',{
    templateUrl:'html/timer.html',
    controller:'timerCtrl',
    refreshOnSearch:false
    })
    .when('/timer/:hours/:minutes/:seconds',{
        templateUrl:'html/timer.html',
        controller:'timerCtrl',
        refreshOnSearch:false
    })
    .when('/worldClock',{
    templateUrl:'html/worldClock.html',
    controller:'worldClockCtrl',
        refreshOnSearch:false
    })
    .when('/worldClock/:id/:name',{
        templateUrl:'html/worldClock.html',
        cntroller:'worldClockCtrl',
        refreshOnSearch:false
    })
});